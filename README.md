# Komputer Store

## Author
Oliver Rimmi

## Description
This application is a dynamic webpage. The functionality include the ability to "work" and receive money, "bank" to send salary to a bank account, "loan" loan money from the bank, "Pay loan" all of the salary is sent to pay off existing loan, the ability select computers to view and receive information about and "buy" where the money in the bank can be used to purchase selected computer. 

![alt text](gui.PNG)

## To run the app:
1. Fork Repo.
2. Pull/Clone fork repo. (alt; download the repo and extract the files).
2. Open local index.html in web-browser.