// Protect variables and functions from global scope
;(function () {
  // Get elements
  const workButton = document.getElementById("workButton")
  const bankButton = document.getElementById("bankButton")
  const loanButton = document.getElementById("loanButton")
  const workBalanceElement = document.getElementById("workBalance")
  const bankBalanceElement = document.getElementById("bankBalance")
  const debtElement = document.getElementById("debt")
  const debtElements = document.querySelectorAll(".debtInfo")
  const workContainer = document.getElementById("workContainer")
  const laptopImageElement = document.getElementById("computerImage")
  const laptopNameElement = document.getElementById("laptopName")
  const laptopInfoElement = document.getElementById("laptopInformation")
  const laptopPriceElement = document.getElementById("laptopPrice")
  const purchaseButton = document.getElementById("purchaseButton")
  const selectComputerElement = document.getElementById("computers")
  const specElement = document.getElementById("specs")

  // Declare variables
  let workBalance = 0
  let bankBalance = 0
  let debt = 0
  let fetchedData = 0
  let selectedComputer = null

  // Create eventHandlers
  const handleWork = () => {
    workBalanceElement.textContent = workBalance += 100
  }

  const handleBankTransfer = () => {
    let debtPayment = workBalance * 0.1
    payDebt(debtPayment)
    if (debt === 0) {
      hideDebtInfo()
      removePayLoanButton()
    }
    transferToBank()
  }

  const handleGetLoan = () => {
    if (debt > 0) {
      alert("You already have a bank loan")
    } else {
      const requestedLoan = parseFloat(prompt("Enter amount: "))

      if (!isNaN(requestedLoan)) {
        if (requestedLoan > bankBalance * 2) {
          alert("You are not allowed to loan that much")
        } else {
          bankBalance += requestedLoan
          bankBalanceElement.textContent = bankBalance
          debt = requestedLoan
          debtElement.textContent = debt
          displayDebtInfo()
          addPayLoanButton()
        }
      }
    }
  }

  const handlePayLoan = () => {
    if (workBalance != 0) {
      payDebt(workBalance)
      transferToBank()

      if (debt === 0) {
        hideDebtInfo()
        removePayLoanButton()
      }
    }
  }

  const handleLaptopDataUpdate = (e) => {
    fetchedData.forEach((element) => {
      if (element.title === e.target.value) {
        updateLaptopData(element)
        updateFeatures(element)
      }
    })
  }

  const handlePurchase = () => {
    const computerPrice = selectedComputer.price
    if (bankBalance >= computerPrice) {
      bankBalance -= computerPrice
      bankBalanceElement.textContent = bankBalance
      const message = `You have successfully purchased "${selectedComputer.title}" for ${computerPrice} SEK. `
      alert(message)
    } else {
      alert("You do not have enough funds")
    }
  }

  // Add eventHandlers
  workButton.addEventListener("click", handleWork)
  bankButton.addEventListener("click", handleBankTransfer)
  loanButton.addEventListener("click", handleGetLoan)
  selectComputerElement.addEventListener("change", handleLaptopDataUpdate)
  purchaseButton.addEventListener("click", handlePurchase)

  // Helper functions
  function displayDebtInfo() {
    debtElements.forEach((element) => {
      element.style.display = "inline"
    })
  }

  function hideDebtInfo() {
    debtElements.forEach((element) => {
      element.style.display = "none"
    })
  }

  function payDebt(debtPayment) {
    if (debtPayment > debt) {
      // Pay off debt and send rest to bankBalance
      debtPayment = debt
      debt = 0
      workBalance -= debtPayment
      debtElement.textContent = debt
    } else {
      // Send money towards paying off debt
      workBalance -= debtPayment
      debt -= debtPayment
      debtElement.textContent = debt
    }
  }

  function transferToBank() {
    bankBalanceElement.textContent = bankBalance += workBalance
    workBalance = 0
    workBalanceElement.textContent = workBalance
  }

  function addPayLoanButton() {
    const payLoanButton = document.createElement("button")
    payLoanButton.innerText = "Repay Loan"
    payLoanButton.setAttribute("id", "payLoanButton")
    payLoanButton.addEventListener("click", handlePayLoan)
    workContainer.appendChild(payLoanButton)
  }

  function removePayLoanButton() {
    payLoanButton = document.getElementById("payLoanButton")
    if (payLoanButton != null) workContainer.removeChild(payLoanButton)
  }

  function updateLaptopData(laptop) {
    laptopNameElement.textContent = laptop.title

    // TODO: Change API response on laptop.image === "assets/images/5.jpg"
    // to "assets/images/5.png".
    if (laptop.id === 5) {
      let sourceParts = laptop.image.split(".")
      laptopImageElement.src = `https://noroff-komputer-store-api.herokuapp.com/${sourceParts[0]}.png`
    } else {
      laptopImageElement.src = `https://noroff-komputer-store-api.herokuapp.com/${laptop.image}`
    }

    laptopInfoElement.textContent = laptop.description
    laptopPriceElement.textContent = `${laptop.price} SEK`
    selectedComputer = laptop
  }

  function initializeLaptopSelection() {
    fetchedData.forEach((element) => {
      const option = document.createElement("option")
      option.textContent = element.title
      option.setAttribute("id", element.id)
      selectComputerElement.appendChild(option)

      updateFeatures(element)
    })
  }

  function updateFeatures(laptop) {
    let specString = ""
    laptop.specs.forEach((spec) => {
      specString += `${spec}\n`
    })
    specElement.textContent = specString
  }

  // Used to fetch and initialize all laptop information
  const load = async () => {
    try {
      const response = await fetch(
        "https://noroff-komputer-store-api.herokuapp.com/computers"
      )
      fetchedData = await response.json()
      updateLaptopData(fetchedData[0])
      initializeLaptopSelection()
    } catch (error) {
      console.error("Something went wrong", error)
    }
  }

  load()
})()
